#include "ArduinoDigitalControl.h"
#define LOG_SCOPE "ArduinoDigital"
#include "logging.hpp"

#include <chrono>
#include <thread>

#ifdef __linux__
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#endif

ArduinoDigitalControl::ArduinoDigitalControl(std::string _p) {
    setSerialPort(_p);
    connected = false;
    disconnect_cb = NULL;
}

ArduinoDigitalControl::~ArduinoDigitalControl() {
    resetPins();

    disconnect();
}

#ifdef _WIN32
#define CLOSEPORT() do {                                                \
        CloseHandle(portHandle);                                        \
        portHandle = INVALID_HANDLE_VALUE;                              \
    } while(0) // swallow semicolon
#elif __linux__
#define CLOSEPORT() do {                                                \
        close(portHandle);                                              \
        portHandle = -1;                                                \
    } while(0) // swallow semicolon
#else
#error fixme
#endif

#define CHECK_WRITECOMMAND(cmd) do {            \
        if(!writeCommand(cmd)) {                \
            return false;                       \
        }                                       \
    } while(0)

#define CHECK_WRITECOMMAND_AND_CLOSE(cmd) do {  \
        if(!writeCommand(cmd)) {                \
            CLOSEPORT();                        \
            return false;                       \
        }                                       \
    } while(0)

#define WAITDONE_EX(err_return, tmo, fail) do {                         \
        int resp = readResponse("Done\r\n", "E000\r\n", (tmo));         \
        if(resp == -2) {                                                \
            errorString = "Timed out. "                                 \
                "COM port is probably disconnected. "                   \
                "Please check connection and device power-on state.";   \
            ERROR(errorString);                                         \
            CLOSEPORT();                                                \
            return (err_return); /* always fail */                      \
        }                                                               \
        else if(resp < 0) {                                             \
            errorString = "Could not communicate with device. "         \
                "Is the device an OZ Optics stage?";                    \
            ERROR(errorString);                                         \
            if(fail) {                                                  \
                CLOSEPORT();                                            \
                return (err_return);                                    \
            }                                                           \
        } else if(resp > 0) {                                           \
            errorString = "Device indicated an error. Continuing.";     \
            WARN(errorString);                                          \
        }                                                               \
    } while(0)

#define WAITDONE() WAITDONE_EX(false, 5000, false)
#define WAITDONE_TMO(tmo) WAITDONE_EX(false, tmo, false)

#define WAITDONE_FAIL() WAITDONE_EX(false, 5000, true)
#define WAITDONE_FAIL_TMO(tmo) WAITDONE_EX(false, tmo, true)

bool ArduinoDigitalControl::disconnect() {
    if(connected) {
        // kill watchdog petter
        TRACE("killing watchdog petter");
        stop_petting_watchdog = true;
        if(watchdog_pet_thread.joinable())
            watchdog_pet_thread.join();
        TRACE("watchdog pet stopped");

        CLOSEPORT();
    }
    connected = false;
    return true;
}

bool ArduinoDigitalControl::connect() {
    return connectWithProgress(NULL, NULL);
}

bool ArduinoDigitalControl::connectWithProgress(void (*progress_cb)(void *data, const char *str, int step, int total), void *cb_data) {
    const int n_steps = 3;
#define PROGRESS_CB(str, step)                          \
    do {                                                \
        if(progress_cb)                                 \
            progress_cb(cb_data, str, step, n_steps);   \
    } while(0)

    PROGRESS_CB("Opening port...", 1);

#ifdef _WIN32
    //Try to open the port
    portHandle = CreateFileA(RS232Device::mungePortName(port).c_str(),
                             GENERIC_READ | GENERIC_WRITE,
                             0,
                             0,
                             OPEN_EXISTING,
                             FILE_ATTRIBUTE_NORMAL,
                             0);


    if(portHandle == INVALID_HANDLE_VALUE) {
        errorString = "Error code " + std::to_string(GetLastError()) + ". Unable to connect to port " + port +
            ". Ensure that the device is indeed connected to this port. "
            "Double-check that the device has been power-cycled since the most recent PC boot-up. "
            "Is another application (e.g. PuTTY) using the port?";
        WARN(errorString);
        return false;
    }

    // Set the Device Control Block
    DCB dcb;
    ZeroMemory(&dcb, sizeof(dcb));
    dcb.DCBlength = sizeof(dcb);
    if(!BuildCommDCBA("9600,n,8,1", &dcb)) {
        // Error building DCB
        CloseHandle(portHandle);
        portHandle = INVALID_HANDLE_VALUE;
        errorString = getWindowsErrorString();
        WARN(errorString);
        return false;
    }

    if(!SetCommState(portHandle, &dcb)) {
        // Error setting DCB parameters
        CloseHandle(portHandle);
        portHandle = INVALID_HANDLE_VALUE;
        errorString = getWindowsErrorString();
        WARN(errorString);
        return false;
    }

    COMMTIMEOUTS to;
    to.ReadIntervalTimeout = MAXDWORD; // 15s interval between bytes read
    to.ReadTotalTimeoutConstant = 0; // 30s total per read call
    to.ReadTotalTimeoutMultiplier = 0;
    to.WriteTotalTimeoutConstant = 500;
    to.WriteTotalTimeoutMultiplier = 0;

    if(!SetCommTimeouts(portHandle, &to)) {
        CloseHandle(portHandle);
        portHandle = INVALID_HANDLE_VALUE;
        errorString = getWindowsErrorString();
        WARN(errorString);
        return false;
    }
#elif __linux__
    //Try to open the port
    portHandle = open(port.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);

    if(portHandle < 0) {
        errorString = "Unable to connect to port " + port + ". Is the port name typed correctly? ";
        errorString += "Did you run the program with read/write permissions?";
        WARN(errorString);
        return false;
    }

    struct termios tio, orig_tio;

    if(tcgetattr(portHandle, &orig_tio) < 0) {
        errorString = "Unable to get TCAttr?";
        WARN(errorString);
        return false;
    }
    memcpy(&tio, &orig_tio, sizeof(struct termios));

    tio.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR
                     | IGNCR | ICRNL | IXON);
    tio.c_oflag &= ~OPOST;
    tio.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tio.c_cflag &= ~(CSIZE | PARENB | CSTOPB);

    tio.c_cflag |= CS8;
    tio.c_cc[VTIME] = 1;

    tio.c_cc[VMIN] = 1;
    if((cfsetospeed(&tio, B9600) & cfsetispeed(&tio, B9600)) < 0) {
        errorString = "Unable to set baud rate";
        WARN(errorString);
        return false;
    };

    if(tcsetattr(portHandle, TCSANOW, &tio) < 0) {
        errorString = "Unable to set attributes";
        WARN(errorString);
        return false;
    }
#endif

    PROGRESS_CB("Waiting for connection...", 2);

    if(readResponse("ARDUINO OK\r\n", "E000", 5000) != 0) {
        CLOSEPORT();
        errorString = "Initial device handshake failed. Is correct serial port selected?";
        ERROR(errorString);
        return false;
    }

    PROGRESS_CB("Initializing...", 3);

    // get version
    writeCommand("V");
    WAITDONE_FAIL();

    versionString = parseVersionString(response);
    DEBUG("Arduino version string " << versionString);

    // get pins
    writeCommand("L");
    WAITDONE_FAIL();

    TRACE("Got response " << response);

    parsePinsFromResponse(response);

    for(auto x : getPins()) TRACE(x);

    connected = true;

    // pull all pins low to start
    resetPins();

    // start watchdog pet
    stop_petting_watchdog = false;
    if(watchdog_pet_thread.joinable())
        watchdog_pet_thread.join();
    watchdog_pet_thread = std::thread(&ArduinoDigitalControl::watchdogPetWorker, this);

    return true;
}

void ArduinoDigitalControl::parsePinsFromResponse(std::string resp) {
    // filter for only 0-9 and whitespace
    std::string filtered;

    for(int i = 0; i < resp.length(); i++) {
        char c = resp[i];
        if(isdigit(c) || c == ' ')
            filtered += c;
    }

    TRACE("Filtered: " << filtered);

    std::stringstream ss(filtered);

    pins.clear();

    int n;
    while(ss >> n)
        pins.push_back(n);
}

bool ArduinoDigitalControl::resetPins() {
    if(connected) {
        std::unique_lock<std::mutex> lock(_mutex);

        writeCommand("R");
        return readResponse("Done\r\n", "E000", 5000) == 0;
    }
    return false;
}

bool ArduinoDigitalControl::digitalWrite(int line, int state) {
    if(connected) {
        std::unique_lock<std::mutex> lock(_mutex);

        if(0 <= line && line < pins.size()) {
            writeCommand("P" + std::to_string(line) + std::to_string(state));
            return readResponse("Done\r\n", "E000", 5000) == 0;
        }
    }

    return false;
}

bool ArduinoDigitalControl::keepAlive() {
    if(connected) {
        std::unique_lock<std::mutex> lock(_mutex);

        writeCommand("K");
        // either response works as a ping (expect E000 in case of an
        // old firmware version, which we don't want to break
        // compatibility with)
        return readResponse("Done\r\n", "E000", 5000) >= 0;
    }

    return false;
}

void ArduinoDigitalControl::registerDisconnectCallback(void (*cb)(void *data, const char *str), void *data) {
    this->disconnect_cb = cb;
    this->disconnect_cb_data = data;
}

void ArduinoDigitalControl::watchdogPetWorker() {
    TRACE("watchdog pet thread started");

    constexpr auto wdog_pet_period = std::chrono::milliseconds(200);

    while(!this->stop_petting_watchdog) {
        if(!keepAlive()) {
            ERROR("watchdog pet failed - connection lost.");

            if(this->disconnect_cb) {
                this->disconnect_cb(disconnect_cb_data, "connection lost");
            }

            CLOSEPORT();
            connected = false;
            return;
        }

        TRACE("received watchdog keepalive ping");

        std::this_thread::sleep_for(wdog_pet_period);
    }

    TRACE("exiting watchdog pet thread");
}

std::string ArduinoDigitalControl::parseVersionString(std::string response) {
    return response.substr(0, response.find("\r\n"));
}

std::string ArduinoDigitalControl::getVersionString() {
    if(versionString == "E000") {
        // prior to addition of firmware versioning - call it v1.0.1
        return "v1.0.1";
    }
    return versionString;
}
