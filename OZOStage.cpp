#include "OZOStage.h"
#define LOG_SCOPE "OZO Stage"
#include "logging.hpp"

// we don't set steps yet
OZOStage::OZOStage(std::string port) : MotorizedStage(port, -1, -1) {
    stageType = OZO;
}

OZOStage::~OZOStage() {
    if(connected) {
        disconnect();
    }
}

void OZOStage::setComPort(std::string cp) {
    // TRACE("in OZOStage::setComPort");
    MotorizedStage::setComPort(cp);
}

#ifdef _WIN32
#define CLOSEPORT() do {                                                \
        CloseHandle(portHandle);                                        \
        portHandle = INVALID_HANDLE_VALUE;                              \
    } while(0) // swallow semicolon
#elif __linux__
#define CLOSEPORT() do {                                                \
        close(portHandle);                                              \
        portHandle = -1;                                                \
    } while(0) // swallow semicolon
#else
#error fixme
#endif

#define CHECK_WRITECOMMAND(cmd) do {            \
        if(!writeCommand(cmd)) {                \
        return false;                           \
    }                                           \
    } while(0)

#define CHECK_WRITECOMMAND_AND_CLOSE(cmd) do {  \
        if(!writeCommand(cmd)) {                \
            CLOSEPORT();                        \
            return false;                       \
        }                                       \
    } while(0)

#define WAITDONE_EX(err_return, tmo, fail) do {                         \
        int resp = readResponse("Done\r\n", "E000", (tmo));             \
        if(resp == -2) {                                                \
            errorString = "Timed out. "                                 \
                "COM port is probably disconnected. "                   \
                "Please check connection and device power-on state.";   \
            ERROR(errorString);                                         \
            CLOSEPORT();                                                \
            return (err_return); /* always fail */                      \
        }                                                               \
        else if(resp < 0) {                                             \
            errorString = "Could not communicate with device. "         \
                "Is the device an OZ Optics stage?";                    \
            ERROR(errorString);                                         \
            if(fail) {                                                  \
                CLOSEPORT();                                            \
                return (err_return);                                    \
            }                                                           \
        } else if(resp > 0) {                                           \
            errorString = "Device indicated an error. Continuing.";     \
            WARN(errorString);                                          \
        }                                                               \
    } while(0)

#define WAITDONE() WAITDONE_EX(false, 5000, false)
#define WAITDONE_TMO(tmo) WAITDONE_EX(false, tmo, false)

#define WAITDONE_FAIL() WAITDONE_EX(false, 5000, true)
#define WAITDONE_FAIL_TMO(tmo) WAITDONE_EX(false, tmo, true)

bool OZOStage::connect() {
    return connectEx();
}

// FIXME (FW): will this crash if progress is NULL?
bool OZOStage::connectEx(void (*progress)(void*, const char *, int, int), void *cb_data) {
    // TRACE("in OZOStage::connect");
    const int n_steps = 4;
#ifdef _WIN32
    progress(cb_data, "Connecting to serial port...", 1, n_steps);
    //Try to open the port
    portHandle = CreateFileA(RS232Device::mungePortName(port).c_str(),
                             GENERIC_READ | GENERIC_WRITE,
                             0,
                             0,
                             OPEN_EXISTING,
                             FILE_ATTRIBUTE_NORMAL,
                             0);


    if(portHandle == INVALID_HANDLE_VALUE) {
        errorString = "Unable to connect to port " + port +
            ". Ensure that the device is indeed connected to this port. "
            "Double-check that the device has been power-cycled since the most recent PC boot-up. "
            "Is another application (e.g. PuTTY) using the port?\n\nWin32 error: " + RS232Device::getWindowsErrorString();
        WARN(errorString);
        return false;
    }

    progress(cb_data, "Setting device parameters...", 2, n_steps);

    // Set the Device Control Block
    DCB dcb;
    ZeroMemory(&dcb, sizeof(dcb));
    dcb.DCBlength = sizeof(dcb);
    if(!BuildCommDCBA("9600,n,8,1", &dcb)) {
        // Error building DCB
        CloseHandle(portHandle);
        portHandle = INVALID_HANDLE_VALUE;
        errorString = getWindowsErrorString();
        WARN(errorString);
        return false;
    }

    if(!SetCommState(portHandle, &dcb)) {
        // Error setting DCB parameters
        CloseHandle(portHandle);
        portHandle = INVALID_HANDLE_VALUE;
        errorString = getWindowsErrorString();
        WARN(errorString);
        return false;
    }

    COMMTIMEOUTS to;
    to.ReadIntervalTimeout = MAXDWORD; // 15s interval between bytes read
    to.ReadTotalTimeoutConstant = 0; // 30s total per read call
    to.ReadTotalTimeoutMultiplier = 0;
    to.WriteTotalTimeoutConstant = 500;
    to.WriteTotalTimeoutMultiplier = 0;

    if(!SetCommTimeouts(portHandle, &to)) {
        CloseHandle(portHandle);
        portHandle = INVALID_HANDLE_VALUE;
        errorString = getWindowsErrorString();
        WARN(errorString);
        return false;
    }
#elif __linux__
    //Try to open the port
    progress(cb_data, "Connecting to serial port...", 1, n_steps);

    portHandle = open(port.c_str(), O_RDWR | O_NOCTTY);

    if(portHandle < 0) {
        errorString = "Unable to connect to port " + port;
        WARN(errorString);
        return false;
    }

    progress(cb_data, "Setting device parameters...", 2, n_steps);

    struct termios tio, orig_tio;

    if(tcgetattr(portHandle, &orig_tio) < 0) {
        errorString = "Unable to get TCAttr?";
        WARN(errorString);
        return false;
    }
    memcpy(&tio, &orig_tio, sizeof(struct termios));

    tio.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR
                     | IGNCR | ICRNL | IXON);
    tio.c_oflag &= ~OPOST;
    tio.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tio.c_cflag &= ~(CSIZE | PARENB);
    tio.c_cc[VTIME] = 1;

    tio.c_cc[VMIN] = 0;
    if((cfsetospeed(&tio, B9600) & cfsetispeed(&tio, B19200)) < 0) {
        errorString = "Unable to set baud rate";
        WARN(errorString);
        return false;
    };
#endif
#if 0
    // set baud rate 9600 bps first!
    CHECK_WRITECOMMAND_AND_CLOSE("B1");
    WAITDONE_FAIL();
#endif

    // make sure echo is turned off!
    CHECK_WRITECOMMAND_AND_CLOSE("E0");
    WAITDONE_FAIL();

    INFO("Got response: " << response);

    // find home and end positions - required command to move stage absolute!
    CHECK_WRITECOMMAND_AND_CLOSE("FH");

    progress(cb_data, "Homing stage...", 3, n_steps);

    WAITDONE_FAIL_TMO(30000);

    // now move to end
    CHECK_WRITECOMMAND_AND_CLOSE("GF");

    progress(cb_data, "Finding end position...", 4, n_steps);

    WAITDONE_FAIL_TMO(30000);

    // query location of end
    CHECK_WRITECOMMAND_AND_CLOSE("S?");
    WAITDONE_FAIL();

    int end_steps = parsePosition(response);
    if(end_steps < 0) {
        errorString = "Unable to query end position (\"" + response + "\")";
        ERROR(errorString);
        CLOSEPORT();
        return false;
    }

    INFO("Maximum step: " << end_steps);
    setSteps(end_steps);

    // calculate length in mm
    const int steps_per_inch = 16384 * 10; // ODL-650-MC
    const double steps_per_mm = steps_per_inch / 25.4;
    double end_len = end_steps / steps_per_mm;

    INFO("Calculated maximum one-way length: " << end_len << " mm");
    setLength(end_len);

    connected = true;

    currentPosition = getCurrentPosition();

    DEBUG("Successfully connected OZ Optics Stage to port: " << port);
    DEBUG("Current position: " << currentPosition);
    return true;
}

bool OZOStage::disconnect() {
    // TRACE("in OZOStage::disconnect");
    if(!connected) {
        WARN("Tried to disconnect from already disconnected stage.");
        return false;
    }

    CLOSEPORT();

    connected = false;
    DEBUG("Successfully disconnected from OZO stage");
    return true;
}

bool OZOStage::moveStageAbsolute(int position) {
    // TRACE("in OZOStage::moveStageAbsolute(" << position << ")");
    if(!connected) {
        errorString = "Tried to move the stage abs, but the stage is not connected";
        ERROR(errorString);
        return false;
    }

    if(position > steps) {
        WARN("Tried to move the stage past max limit (" << position << " < " << steps << "). Going to max instead.");
        return moveStageAbsolute(steps);
    }
    else if(position < 0) {
        WARN("Tried to move the stage past 0 limit. Going to 0 instead.");
        return moveStageAbsolute(0);
    }

    // query stage status first
    do {
        if(!writeCommand("Q?")) {
            return false;
        }
        WAITDONE();

        if(response.find("BUSY") != std::string::npos) {
            WARN("Stage status is BUSY. Unable to move stage.");
            return false;
        }
#if 0 // again - WHY!?
        else if(response.find("HOME") != std::string::npos) {
            WARN("Stage status is HOME. Stage is being reset.");
            writeCommand("RESET");
            readResponse("Ready\r\n");
            powerReset();
            return false;
        }
#endif
    } while (response.find("IDLE") == std::string::npos &&          // the response must contain one of the four valid statuses!!!
             response.find("BUSY") == std::string::npos &&
             response.find("HOME") == std::string::npos &&
             response.find("END") == std::string::npos);

    // absolute move
    if(!writeCommand("S" + std::to_string(position))) {
        return false;
    }
    //WAITDONE();

    //currentPosition = position;
    // INFO("Stage now at " << currentPosition);
    return true;
}

bool OZOStage::moveStageAbsoluteMM(double position) {
    int32_t target = position / length * steps;
    return moveStageAbsolute(target);
}

bool OZOStage::moveStageForward(double distance) {
    // TRACE("in OZOStage::moveStageForward");
    // convert distance for mm to steps
    int32_t nSteps = (int32_t)(distance / length * steps);
    return moveStageAbsolute(currentPosition + nSteps);
}

bool OZOStage::moveStageBackward(double distance) {
    // TRACE("in OZOStage::moveStageBackward");
    return moveStageForward(-distance);
}

bool OZOStage::resetStage() {
    // TRACE("in OZOStage::resetStage");
    if(!connected) {
        errorString = "Tried to reset a disconnected stage.";
        WARN(errorString);
        return false;
    }

    // move to 0 position
    return moveStageAbsolute(0);
}

bool OZOStage::goToHome() {
    // TRACE("in OZOStage::goToHome");
    return MotorizedStage::goToHome();
}

bool OZOStage::setCurrentAsHome() {
    // TRACE("in OZOStage::setCurrentAsHome");
    return MotorizedStage::setCurrentAsHome();
}

// parse the response of a S? command
int OZOStage::parsePosition(std::string resp) {
    std::string position = "";
    bool parsing;
    for (int i = 0; i < resp.length(); ++i) {
        if(isdigit(resp[i])) {
            position += resp[i];
            parsing = true;
        }
        else {
            parsing = false;
        }

        if(position.length() > 0 && !parsing) {
            break;
        }
    }

    if(position.length() == 0) {
        return NULL_STAGE_POSITION;
    }

    return std::stoi(position);
}

enum OZOStage::status_t OZOStage::getStatus() {
    // try up to 10 times
    for(int i = 0; i < 10; i++) {
        if(!writeCommand("Q?")) {
            break;
        }
        WAITDONE_EX(OZOStage::UNKNOWN, 5000, false);

        if(response.find("IDLE") != std::string::npos)
            return OZOStage::IDLE;
        if(response.find("BUSY") != std::string::npos)
            return OZOStage::BUSY;
        if(response.find("HOME") != std::string::npos)
            return OZOStage::HOME;
        if(response.find("END") != std::string::npos)
            return OZOStage::END;
    }
    return OZOStage::UNKNOWN;
}

int OZOStage::getCurrentPosition() {
    // TRACE("in OZOStage::getCurrentPosition");
    if(!connected) {
        errorString = "Tried to get current position of a disconnected stage.";
        WARN(errorString);
        return NULL_STAGE_POSITION;
    }

#if 0
    // query stage status first
    do {
    } while (response.find("BUSY") != std::string::npos);   // keep querying until not busy
#endif

#if 0
    // why tf would you do this!?
    if(response.find("HOME") != std::string::npos) {
        WARN("Stage status is HOME. Stage is being reset.");
        writeCommand("RESET");
        readResponse("Ready\r\n");
        powerReset();
        return 0;
    }
#endif

    // query position
    do {
        if(!writeCommand("S?")) {
            return NULL_STAGE_POSITION;
        }
        WAITDONE();
    } while (response.find("STEP") == std::string::npos);   // status must contain STEP because that proceeds the position!!!

    if(response.empty()) {
        return NULL_STAGE_POSITION;
    }

    return parsePosition(response);
}

double OZOStage::getCurrentPositionMM() {
    // TRACE("in OZOStage::getCurrentPositionMM");
    return MotorizedStage::getCurrentPositionMM();
}

void OZOStage::setHome(int32_t hm) {
    // TRACE("in OZOStage::setHome");
    return MotorizedStage::setHome(hm);
}

bool OZOStage::powerReset() {
    TRACE("Disconnecting stage.");
    if(!disconnect()) {
        WARN("Disconnection failed.");
        return false;
    }
    INFO("Re-connecting stage.");
    if(!connect()) {
        WARN("Re-connection failed.");
        return false;
    }
    return true;
}
