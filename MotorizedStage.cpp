#include "MotorizedStage.h"
#define LOG_SCOPE "Motorized Stage"
#include "logging.hpp"

MotorizedStage::MotorizedStage(std::string comPort, double len, uint32_t step) {
    // TRACE("in MotorizedStage constructor");
    errorString = "No error";
    port = comPort;
    length = len;
    steps = step;
    connected = false;
    currentPosition = 0;
    home = 0;
    acqSynced = false;
    syncStartPos = 0;
}


MotorizedStage::~MotorizedStage() {
    // TRACE("in MotorizedStage destructor");

}


bool MotorizedStage::setCurrentAsHome() {
    // TRACE("in MotorizedStage::setCurrentAsHome");
    int32_t data = getCurrentPosition();
    if (data != NULL_STAGE_POSITION) {
        home = data;
        INFO("Home set to " << data);
        return true;
    }
    else {
        errorString = "Unable to set the home";
        WARN(errorString);
        return false;
    }
}
bool MotorizedStage::goToHome() {
    // TRACE("in MotorizedStage::goToHome");
    INFO("Returning to home position " << home);
    return moveStageAbsolute(home);
}

void MotorizedStage::setComPort(std::string p) {
    // TRACE("in MotorizedStage::setComPort");
    port = p;
    // INFO("Com port changed to " << p);
}

double MotorizedStage::getCurrentPositionMM() {
    // TRACE("in MotorizedStage::getCurrentPositionMM");
    currentPosition = getCurrentPosition();
    // INFO("Get current position in mm called. Current position is " << (double)pos/steps * length);
    if (currentPosition != NULL_STAGE_POSITION) {
        return fabs((double)currentPosition) / steps * length;  //always return a positive number
    }
    return NULL_STAGE_POSITION;
}

double MotorizedStage::getLength() const {
    return length;
}

int32_t MotorizedStage::getHome() const {
    // negate NAI stage return value because its movements are flipped
    if (stageType == NAI) {
        return -home;
    }
    // must be a Zaber or OZO stage then
    return home;
}

int32_t MotorizedStage::getSteps() const {
    return steps;
}

void MotorizedStage::setHome(int32_t hm) {
    // TRACE("in MotorizedStage::setHome");
    home = stageType == NAI && hm > 0 ? -hm : hm;
}

void MotorizedStage::setLength(double l) {
    // TRACE("in MotorizedStage::setLength");
    length = l;
}

void MotorizedStage::setSteps(int32_t s) {
    // TRACE("in MotorizedStage::setSteps");
    steps = s;
}

bool MotorizedStage::isAcqSynced() const {
    // TRACE("in MotorizedStage::isAcqSynced");
    return acqSynced;
}

void MotorizedStage::setAcqSynced(bool sync) {
    // TRACE("in MotorizedStage::setAcqSynced");
    acqSynced = sync;
    // set start position for synced movement if acquisition sync turned on
    if (acqSynced) {
        syncStartPos = currentPosition * length / (double)steps;
        // DEBUG("syncStartPos=" << syncStartPos << " mm");
    }
}


double MotorizedStage::getSyncStartPos() const {
    return syncStartPos;
}


MotoStageType MotorizedStage::getStageType() const {
    return stageType;
}


std::string MotorizedStage::getStageTypeStr() const {
    switch (stageType) {
    case Zaber:
        return "Zaber";
    case NAI:
    case NAI_USB:
        return "NAI";
    case OZO:
        return "OZ Optics";
    case NST:
        return "New Scale";
    default:
        WARN("Tried to get string of unknown stage type");
        return "";
    }
}
