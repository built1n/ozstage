#include "OZOStage.h"
#include <iostream>
using namespace std;

int main() {
    OZOStage *stage = new OZOStage("COM8");

    stage->connect();

    if(!stage->isConnected()) {
        cerr << "Failed to create stage" << endl;
        return 1;
    }

    for(;;) {
        int current = stage->getCurrentPosition();
        double current_mm = stage->getCurrentPositionMM();
        cout << "Current position: " << current << " (" << current_mm << " mm)" << endl;
        cout << "Enter new position: " << endl;
#if 0
        int pos;
        cin >> pos;
        stage->moveStageAbsolute(pos);
#else
        double pos;
        cin >> pos;
        stage->moveStageAbsoluteMM(pos);
#endif
    }
}
