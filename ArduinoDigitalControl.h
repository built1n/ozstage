#pragma once

#include "RS232Device.h"

#include <cstdint>
#include <mutex>
#include <vector>
#include <thread>

class ArduinoDigitalControl : protected RS232Device {
public:
    ArduinoDigitalControl(std::string p = "");
    ~ArduinoDigitalControl();

    void setSerialPort(std::string p) { port = p; }

    // not thread-safe
    bool connect();
    bool connectWithProgress(void (*progress_cb)(void *data, const char *str, int step, int total) = NULL, void *data = NULL);
    bool disconnect();

    // register callback for unexpected disconnect notification
    void registerDisconnectCallback(void (*disconnect_cb)(void *data, const char *str), void *data);

    bool isConnected() { return connected; }

    // thread-safe
    bool digitalWrite(int line, int state);
    bool resetPins();
    // returns false if no response
    bool keepAlive();

    std::string getError() { return errorString; }
    std::vector<int> getPins() { return pins; }
    int getNumPins() { return pins.size(); }

    std::string getPinName(int line) {
        return "D" + std::to_string(pins[line]);
    }

    std::string getVersionString();

private:
    void parsePinsFromResponse(std::string resp);
    std::string parseVersionString(std::string response);
    std::vector<int> pins;
    std::string errorString, port, versionString;
    std::mutex _mutex;

    bool connected;

    void watchdogPetWorker();
    std::thread watchdog_pet_thread;
    bool stop_petting_watchdog;

    void (*disconnect_cb)(void *data, const char *str);
    void *disconnect_cb_data;
};
