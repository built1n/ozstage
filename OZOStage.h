/*      Origianl Author: Moseph Jackson-Atogi
        Date Created: October 31, 2017
        Description: This file contains sub-class of abstract object MotorizedStage representing an OZ Optics
        motorized stage connected via RS232 serial port and has methods for controlling the stage.
*/
#ifndef __OZOSTAGE_H__
#define __OZOSTAGE_H__

#include "MotorizedStage.h"
#include "RS232Device.h"

class OZOStage : public MotorizedStage, protected RS232Device {
public:
    OZOStage(std::string port);                              // constructor
    ~OZOStage();                                                                                                                    // destructor

    void setComPort(std::string cp);                                                                                // change the com port
    bool connect();
    bool connectEx(void (*progress_cb)(void*, const char*, int, int) = NULL, void* data = NULL);                                                                                                                 // tries to connect to a stage on com port
    bool disconnect();                                                                                                              // tries to disconnect stage from com port
    bool moveStageAbsolute(int postion);                                                                    // move the stage to an absolute position (in steps)
    bool moveStageAbsoluteMM(double position); // in mm
    bool moveStageForward(double distance);                                                                 // moves the stage forward
    bool moveStageBackward(double distance);                                                                // move the stage backward
    bool resetStage();                                                                                                              // reset the stage, usefull for when it loses power
    bool goToHome();                                                                                                                // move the stage to a pre defined home position
    bool setCurrentAsHome();                                                                                                // set a home position for the stage based off current position
    int getCurrentPosition();                                                                                               // get the current position of the stage in ticks
    double getCurrentPositionMM();                                                                                  // get the current position of the stage in millimeters
    void setHome(int32_t hm);                                                                                               // sets protected variable home; called only when a config has been loaded
    int parsePosition(std::string resp);

    enum status_t { UNKNOWN = -1, IDLE, BUSY, HOME, END };

    enum OZOStage::status_t getStatus();

protected:
    bool powerReset();                                                                                                              // disconnects then re-connects the stage, in the case abnormal HOME status occurs

};

#endif // __OZOSTAGE_H__
