#pragma once
#ifdef _WIN32
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <Windows.h>
#endif
#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>
#include <omp.h>

enum SeverityLevel {
	SEV_TRACE,
	SEV_DEBUG,
	SEV_INFO,
	SEV_WARN,
	SEV_ERROR,
	SEV_CRITICAL,
	SEV_EMERGENCY
};
enum color {
	DARKBLUE = 1,
	DARKGREEN,
	DARKTEAL,
	DARKRED,
	DARKPINK,
	DARKYELLOW,
	GRAY,
	DARKGRAY,
	BLUE,
	GREEN,
	TEAL,
	RED,
	PINK,
	YELLOW,
	WHITE
};


void setColor(SeverityLevel s);
std::ostream& log(const char* scope, SeverityLevel level, int line);
std::ostream& filelog(const char* scope, SeverityLevel level, int line);
void endlog();
void openLog();
void locked_log(std::string, SeverityLevel);
std::ostream& operator<<(std::ostream& stream, SeverityLevel level);
std::ostream& convertToSS();

#if !defined(LOG_SCOPE)
#define LOG_SCOPE "default"
#endif

#ifdef ERROR
#undef ERROR
#endif
extern std::mutex mutex_log;
#define LOG(lvl, x) do {mutex_log.lock(); log(LOG_SCOPE, lvl, __LINE__) << x; filelog(LOG_SCOPE, lvl, __LINE__) << x; endlog(); mutex_log.unlock();} while(0)

#define TRACE(x)		LOG(SeverityLevel::SEV_TRACE, x)
#define DEBUG(x)		LOG(SeverityLevel::SEV_DEBUG, x)
#define INFO(x)			LOG(SeverityLevel::SEV_INFO, x)
#define WARN(x)			LOG(SeverityLevel::SEV_WARN, x)
#define ERROR(x)		LOG(SeverityLevel::SEV_ERROR, x)
#define CRITICAL(x) 	LOG(SeverityLevel::SEV_CRITICAL, x)
#define EMERGENCY(x)	LOG(SeverityLevel::SEV_EMERGENCY, x)

#define WARNING(x) WARN(x)
#define EMERG(x) EMERGENCY(x)
