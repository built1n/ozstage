#include "ArduinoDigitalControl.h"

#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    ArduinoDigitalControl *ard = new ArduinoDigitalControl(argv[1]);

    if(!ard->connect()) {
        cerr << ard->getError() << endl;
        return 1;
    }

    while(1) {
        int line, state;
        cin >> line >> state;
        bool success = ard->digitalWrite(line, state);
        if(!success)
            cerr << "Failure" << endl;
    }
}
