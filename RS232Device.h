/*
  Origianl Author: Moseph Jackson-Atogi
  Date Created: October 31, 2017
  Description: This file contains class for any device connected via RS232 serial port and has methods
  for communication with device.
*/

#ifndef __RS232DEVICE_H__
#define __RS232DEVICE_H__
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#elif __linux__
#include <unistd.h>
#endif
#include <cstring>
#include <map>
#include <string>
#include <utility>

class RS232Device {
public:
    RS232Device();                  // constructor
    ~RS232Device();                 // destructor

    static std::string mungePortName(std::string input);

    // strip control characters from response
    static std::string cleanString(std::string str);
protected:
#ifdef _WIN32
    HANDLE portHandle;                                                                                                      // handle to the com port
#elif __linux__
    int portHandle;                                                                                                        // pointer to the com port
#endif
    std::string serialNumber;                                                                                       //stage's serial number
    std::map<std::string, std::pair<double, int>> validSerialNums;          //map that stores valid serial numbers for the stage along with stage length and steps
    std::string command, response;                                                                          // command to be sent to stage and response received from stage

    std::string getWindowsErrorString();                                                            //returns string representation of the last windows error (BK)
    bool writeCommandData(unsigned char* cmd, int size);                                        // writes and sends a command to this stage's connected com port (BK)
    bool writeCommand(std::string cmd, bool append_crlf = true);

    // return values:
    // -2: fatal communication error (timeout) - this probably means the port is disconnected
    // -1: nonfatal communication error - try again
    // 0: found `untilFound`
    // 1: found `errorString`
    int readResponse(std::string untilFound = "", std::string errorString = "", int timeout_ms = -1);                                         // reads stage response to a command up until untilFound string is found (if specified, o/w carriage return)
};

#endif // __RS232DEVICE_H__
