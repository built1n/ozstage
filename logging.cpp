#include "logging.hpp"

#include <string.h>
#include <iomanip>
#include <chrono>
#include <algorithm>
#include <sstream>
#if defined _WIN32
#include <direct.h>
#elif defined __GNUC__
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif
static const char* const reset_escape = "\x1b[0m";

#ifdef _WIN32
void setColor(SeverityLevel s) {
	const color colors[] = {
		WHITE,
		GREEN,
		GRAY,
		YELLOW,
		RED,
		DARKRED,
		PINK,
		DARKPINK
	};
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hCon, colors[s]);
}
#endif
const char* color_escape(SeverityLevel level) {
#ifdef __GNUC__
    if(!isatty(STDERR_FILENO))
        return "";
#endif

	// ref: https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
	static const char* const colors[] =
	{
		"\x1b[37;2m",
		"\x1b[34m",
		reset_escape,
		"\x1b[33;1m",
		"\x1b[31;1m",
		"\x1b[37;41;1m",
		"\x1b[37;41;1;4m"
	};

	if (static_cast<std::size_t>(level) < (sizeof(colors) / sizeof(*colors))) {
		return colors[level];
	}
	else {
		return reset_escape;
	}
}

// ref: http://www.boost.org/doc/libs/1_57_0/libs/log/doc/html/log/detailed/expressions.html#log.detailed.expressions.attr_keywords
std::ostream& operator<<(std::ostream& stream, SeverityLevel level) {
	static const char* const names[] =
	{
		"TRCE",
		"DEBG",
		"INFO",
		"WARN",
		"ERRR",
		"CRIT",
		"EMRG"
	};

	if (static_cast<std::size_t>(level) < (sizeof(names) / sizeof(*names))) {
		stream << names[level];
	}
	else {
		stream << static_cast<int>(level);
	}
	return stream;
}

static std::ostream& stream = std::cerr;
std::mutex mutex_log;
auto now = std::chrono::system_clock::now();

static std::ofstream fileStream;
static auto mark = std::chrono::high_resolution_clock::now();

void openLog() {

	//make logging directory if one doesn't exist
	//https://stackoverflow.com/questions/23427804
#if defined _WIN32
	_mkdir("log");
#elif defined __GNUC__
	mkdir("log", 0777);
#endif

	//https://stackoverflow.com/questions/17223096/
	auto in_time_t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::stringstream ss;
	ss << std::put_time(std::localtime(&in_time_t), "%m-%d-%Y_%I_%M_%S");
	std::string fname = std::string("log/") + ss.str() + ".log";
	fileStream.open(fname.c_str(), std::ofstream::out);
	if (!fileStream.is_open()) {
		ERROR("Could not open output file for log with name " << fname);
	}
}

void endlog() {
	stream << std::endl;
	fileStream << std::endl;
#ifdef _WIN32
	setColor(SeverityLevel::SEV_INFO);
#elif __linux__
	stream << color_escape(SeverityLevel::SEV_INFO);
#endif
}

std::ostream& log(const char* scope, SeverityLevel level, int line) {
	// keep track of the longest scope seen this far
	static size_t longest_scope = 0;
	longest_scope = std::max(longest_scope, strlen(scope));

	auto now = std::chrono::high_resolution_clock::now();
	double time = std::chrono::duration_cast<std::chrono::microseconds>(now - mark).count() / 1e6;

	// log the front matter
	stream << std::right << std::setprecision(6) << std::fixed << time << std::resetiosflags(std::ios::fixed);
	stream << " [";
	size_t i;
	for (i = 0; i < (longest_scope - strlen(scope)) / 2; i++) {
		stream << " ";
	}
	stream << scope;
	for (i += strlen(scope); i < longest_scope; i++) {
		stream << " ";
	}
	stream << ":" << line << "] ";

	// log the colored message
#ifdef _WIN32
	setColor(level);

#elif __linux__
	stream << color_escape(level);
#endif

	stream << level << ": ";

	return stream;
}

std::ostream& filelog(const char* scope, SeverityLevel level, int line) {
	// keep track of the longest scope seen this far
	static size_t longest_scope = 0;
	longest_scope = std::max(longest_scope, strlen(scope));

	auto now = std::chrono::high_resolution_clock::now();
	double time = std::chrono::duration_cast<std::chrono::microseconds>(now - mark).count() / 1e6;

	//write out to a file too
	// log the front matter
	fileStream << std::right << std::setprecision(6) << std::fixed << time << std::resetiosflags(std::ios::fixed);
	fileStream << " [";
	size_t i;
	for (i = 0; i < (longest_scope - strlen(scope)) / 2; i++) {
		fileStream << " ";
	}
	fileStream << scope;
	for (i += strlen(scope); i < longest_scope; i++) {
		fileStream << " ";
	}
	fileStream << ":" << line << "] ";

	// log the colored message
	fileStream << level << ": ";

	return fileStream;
}

