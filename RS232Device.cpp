#include "RS232Device.h"
#define LOG_SCOPE "RS232 Stage"
#include "logging.hpp"

RS232Device::RS232Device() {}

RS232Device::~RS232Device() {}


//returns a string representation of the last error
//from http://stackoverflow.com/questions/1387064/how-to-get-the-error-message-from-the-error-code-returned-by-getlasterror
std::string RS232Device::getWindowsErrorString() {
    // TRACE("in RS232Stage::getWindowsErrorString");
#ifdef _WIN32
    //Get the error message, if any.
    DWORD errorMessageID = GetLastError();
    if (errorMessageID == 0) {
        return std::string(); //No error message has been recorded
    }
    LPSTR messageBuffer = nullptr;
    size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                 NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

    std::string message(messageBuffer, size - 2);

    //Free the buffer.
    LocalFree(messageBuffer);

    return message;
#elif __linux__
    return "";
#endif
}

std::string RS232Device::mungePortName(std::string input) {
#ifdef _WIN32
    /*
     * Windows is weird. Really !@#$ing weird. Due to some nasty
     * quirks of WinNT, the technically "correct" way to access COM
     * ports is via the "\\.\COM12" syntax. The bare "COM8" syntax
     * works due to some hacks, instituted (presumably) to maintain
     * backwards-compatibility.
     *
     * We can thankfully work around this nonsense by prepending the
     * port name with "\\.\", as long as the port does not already
     * contain a backslash.
     *
     * This idea is due to Simon Tatham's PuTTY, see windows/winser.c.
     *
     * [1]: https://support.microsoft.com/en-us/help/115831/howto-specify-serial-ports-larger-than-com9
     * [2]: https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file#win32-device-namespaces
     */

    if(input.find("\\") != std::string::npos)
        return input; // string contains a backslash

    return "\\\\.\\" + input;
#elif __linux__
    return input;
#endif
}

bool RS232Device::writeCommandData(unsigned char *cmd, int size) {
    // TRACE("in RS232Stage::writeCommand");
#ifdef _WIN32
    if (portHandle == INVALID_HANDLE_VALUE) {
        ERROR("Invalid port handle");
        return false;
    }
    unsigned long bytesWritten;
    if (!WriteFile(portHandle, cmd, size, &bytesWritten, 0)) {
        WARN(getWindowsErrorString());
        return false;
    }
#elif __linux__
    if (portHandle < 0) {
        ERROR("Invalid port handle");
        return false;
    }
    if (write(portHandle, cmd, size) != size) {
        ERROR(std::string("Unable to write command ") +
              " " + strerror(errno));
        return false;
    }
#endif
    return true;
}

bool RS232Device::writeCommand(std::string cmd, bool append_crlf) {
    DEBUG("Writing command: " << cmd);
    if(append_crlf) {
        // are we already CRLF terminated?
        if(cmd.length() >= 2 && cmd.rfind("\r\n") == cmd.length() - 2){
            WARN("Command is already terminated by CR/LF. Appending twice!");
        }
        cmd += "\r\n";
    }

    return writeCommandData((unsigned char*)cmd.c_str(), cmd.length());
}

static void dump(std::string s) {
    std::cout << "String: '";
    for(int i = 0; i < s.length(); i++) {
        char c = s[i];
        std::string out;
        switch(c) {
        case '\r':
            out = "\\r";
            break;
        case '\n':
            out = "\\n";
            break;
        default:
            out = c;
        }
        std::cout << out;
    }
    std::cout << "'" << std::endl;
}

// reads up to 64 bytes, or to untilFound, or when the timeout is exceeded
int RS232Device::readResponse(std::string untilFound, std::string errorString, int timeout_ms) {
    TRACE("in RS232Stage::readResponse");
    // reset response variable
    response = "";

    int status = -2; // timeout

    // init for loop variables
    unsigned long bytesRead = 0;
    unsigned char c;

    const int delay_time = 50; // 20 Hz

    for (int i = 0; (timeout_ms < 0) || (i < timeout_ms / delay_time); i++) {
#ifdef _WIN32
        //TRACE("Blocking for byte:" << i);
        unsigned long n;
        if (!ReadFile(portHandle, &c, 1, &n, 0)) { // error reading file
            WARN("Read failed - check connection.");
            status = -1; // nonfatal (windows does this sometimes)
            break;
        }
        if (n == 0) {
            Sleep(delay_time);
            //DEBUG("returning due to read EOF");
            //break;
        }

#elif __linux__
        // bulid response
        ssize_t n = read(portHandle, &c, 1);
        if (n < 0 && errno != EWOULDBLOCK) {
            WARN("Read failed - check connection.");
            status = -1;
            break;
        }
        if(n == 0 || (n < 0 && errno == EWOULDBLOCK)) {
            usleep(delay_time * 1000);
        }
#endif

        bytesRead += n;

        // bulid response
        if(n == 1) {
            response += c;
            //DEBUG("Read byte " /*<< c */<< " (" << (int)c << ")");
        }
        //dump(response);
        //dump(untilFound);

        // done if untilFound AND carriage return found
        if (response.find(untilFound) != std::string::npos && response.find('\r') != std::string::npos) {
            status = 0; // success
            //DEBUG("matches untilFound");
            break;
        }

        if (errorString != "" && response.find(errorString) != std::string::npos && response.find('\r') != std::string::npos) {
            status = 1; // device error
            break; // return error
        }
    }
    TRACE("Got response from device: " << response);
    return status;
}

std::string RS232Device::cleanString(std::string str) {
    std::string filtered;
    for(int i = 0; i < str.length(); i++) {
        char c = str[i];
        if(!iscntrl(c))
            filtered += c;
    }
    return filtered;
}
