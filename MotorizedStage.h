/*
  Origianl Author: Brenton Keller
  Date Created: Before February 2017
  Description: This file contains abstract object representing a connected motorized stage/reference arm and
  has methods for controlling the stage.
*/

#pragma once
#include <cmath>
#include <map>
#include <string>
#include <stdint.h>
#include <thread>
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#elif __linux__
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#endif

#define NULL_STAGE_POSITION -9999999

// Describes the type of motorized stage (MOSS)
enum MotoStageType {
    NULL_STAGE = -1,
    Zaber,
    NAI,
    NAI_USB,
    OZO,
    NST
};

// These types represent the various ways in which reference arms are controlled (BK)
enum ReferenceArmModes {
    Reset,                  // reset to 0 position
    Relative,               // move relative
    Absolute,               // move to absolute position
    SetHome,                // set a home position
    GoHome                  // move to home position
};

//abstract base class
class MotorizedStage {
public:
    MotorizedStage(std::string comPort, double len, uint32_t step);         //sets default com port, length of stage and the number of steps
    ~MotorizedStage();
    virtual bool moveStageForward(double distance) = 0;                                                                     //moves the stage forward
    virtual bool moveStageBackward(double distance) = 0;                                                            //move the stage backward
    virtual bool moveStageAbsolute(int postion) = 0;                                                                        //move the stage to an absolute position
    virtual bool resetStage() = 0;                                                                                                          //reset the stage
    virtual bool moveStageAbsoluteMM(double position) = 0;
    bool setCurrentAsHome();                                                                                                                        //set a home position for the stage based off current position
    bool goToHome();                                                                                                                                        //move the stage to a pre defined home position
    std::string getError() { return errorString; };
    void setComPort(std::string);                                                                                                           //change the com port
    virtual bool connect() = 0;                                                                                                                     //tries to connect to a stage on com port port
    virtual bool disconnect() = 0;                                                                                                          //disconnects from the stage
    bool isConnected() const { return connected; }
    virtual int getCurrentPosition() = 0;                                                                                   //get the current position of the stage in ticks
    double getCurrentPositionMM();                                                                                                  //get the current position of the stage in mm
    double getLength() const;                                                                                                               // obtain the maximum length (in mm) of the stage (MOSS)
    int32_t getHome() const;                                                                                                                // obtain current home position (in mm) of stage (MOSS)
    int32_t getSteps() const;                                                                                                               // obtain maximum steps of the stage (MOSS)
    void setHome(int32_t hm);                                                                                                               // sets protected variable home; called only when a config has been loaded (MOSS)
    void setLength(double l);                                                                                                               // sets protected variable length; called when a stage's settings are being changed (MOSS)
    void setSteps(int32_t s);                                                                                                               // sets protected variable steps; called when a stage's settings are being changed (MOSS)
    bool isAcqSynced() const;                                                                                                               // is stage movement synced to an acquisition (MOSS)
    void setAcqSynced(bool sync);                                                                                                   // sets protected variable acqSynced (MOSS)

    double getSyncStartPos() const;                                                                                                 // return value of syncStartPos variable (MOSS)
    MotoStageType getStageType() const;                                                                                             // return value of stageType variable (MOSS)
    std::string getStageTypeStr() const;                                                                                    // return string of stageType (MOSS)
    bool isConnected() { return connected; }

protected:

    MotoStageType stageType;                                                                                                // what type of motorized stage is this (MOSS)
    std::string port;                                                                                                               //what com port the stage is on

    std::string errorString;                                                                                                //what the current error is

    bool connected;                                                                                                                 //are we connected to the stage
    int32_t currentPosition;                                                                                                //where the stage currently is in ticks
    int32_t home;                                                                                                                   //home position for the stage in ticks

    int32_t steps;                                                                                                                  //what are the max number of steps in the stage?
    double length;                                                                                                                  //how long is the stage?
    bool acqSynced;                                                                                                                 // sync stage movement with an acquisition (MOSS)
    double syncStartPos;                                                                                                    // starting stage position for a Spiral scan aquisition sync (MOSS)
};
